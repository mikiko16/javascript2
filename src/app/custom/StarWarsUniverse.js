import Entity from './Entity';

export default class StarWarsUniverse {

    constructor() {
        this.entities = [];

    }

    async init() {

    var opts = {
        method: 'GET',      
        headers: {}
        };
        
    await fetch('https://swapi.booost.bg/api/', opts).
        then(function (response) {
        return response.json();
        })
        .then((body) => {

        for (const [key, value] of Object.entries(body)) {            

            fetch(value, opts).
                then((response) => {
                    return response.json();
                })
                .then((body) => {
                    let newEntity = new Entity(key, body);

                    this.entities.push(newEntity);
                })
          }
    });
    }
}